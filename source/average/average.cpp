#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cmath>

int main(int argc, char **argv) {
	double sum = 0;
	double num = 0;
	long numofsum = 0;
	int row;

	int linefrom, lineto;
	std::istringstream input(argv[1]);
	input >> linefrom;
	std::istringstream input2(argv[2]);
	input2 >> lineto;
	std::istringstream input4(argv[4]);
	input4 >> row;


	//open file
	std::ifstream inputfile(argv[3]);
	std::string line;
	for(int i = 1; i < linefrom; i++)
		std::getline(inputfile, line);
	for(int i = linefrom; i <= lineto; i++){
		for(int i = 1; i <= row; i++)
			inputfile >> num;
		std::getline(inputfile, line);
//		std::cout << num << std::endl;
		sum += num;
		numofsum++;
	}
	double average = sum / numofsum;

	std::cout << average << " +/- " ;
	sum = 0;
	numofsum = 0;

	inputfile.close();
	inputfile.clear();
	inputfile.open(argv[3]);

	for(int i = 1; i < linefrom; i++)
		std::getline(inputfile, line);
	for(int i = linefrom; i <= lineto; i++){
		for(int i = 1; i <= row; i++)
			inputfile >> num;
		std::getline(inputfile, line);
		//std::cout << " " << num << " " << sum << " " << average << std::endl;
		sum += (num - average) * (num - average);
		numofsum++;
	}
	sum = sum / numofsum;
	sum = sqrt(sum);
	std::cout << sum << std::endl;
	inputfile.close();
}
